from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Status
from .forms import Status_Form
from .views import index
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import unittest

# Create your tests here.
class Status_Test(TestCase):
    def test_status_url_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_status_using_index_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        # Creating a new user
        new_status = Status.objects.create(status = "Doing lab 7 with happy smile")

        # Retrieving all available user
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'status': ''})
        self.assertFalse(form.is_valid())

    def test_form(self):
        form = Status_Form(data={'status': "Doing lab 7 with happy smile"})
        self.assertTrue(form.is_valid())

    def test_post(self):
        response = self.client.post('/status/', {'status': "Doing lab 7 with happy smile"})
        self.assertEqual(response.status_code, 200)

class Status_FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Status_FunctionalTest,self).setUp()

    def tearDown(self):
        # self.selenium.implicitly_wait(5)
        self.selenium.quit()
        super(Status_FunctionalTest, self).tearDown()

    def test_can_submit_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/status/')
        # time.sleep(5)

        # find the form element
        search = self.selenium.find_element_by_id('id_status')

        # Fill the form with data
        search.send_keys('Coba Coba')
        search.submit()
        # time.sleep(5)

        self.assertIn("Coba Coba", self.selenium.page_source)

    def test_css_status_post_fontFamily_is_right(self):
        self.selenium.get('http://127.0.0.1:8000/status/')
        fontFamily = self.selenium.find_element_by_class_name("status-post").value_of_css_property("font-family")
        self.assertEqual(fontFamily, "Kelson")

    def test_css_status_date_fontFamily_is_right(self):
        self.selenium.get('http://127.0.0.1:8000/status/')
        fontFamily = self.selenium.find_element_by_class_name("status-date").value_of_css_property("font-family")
        self.assertEqual(fontFamily, "Montserrat")

    def test_greeting_at_the_right_coordinate(self):
        self.selenium.get('http://127.0.0.1:8000/status/')
        greeting = self.selenium.find_element_by_id('greeting')
        location = greeting.location
        coor_X = location['x']
        coor_Y = location['y']
        self.assertEqual(coor_X, 110)
        self.assertEqual(coor_Y, 120)

    def test_status_list_header_at_the_right_coordinate(self):
        self.selenium.get('http://127.0.0.1:8000/status/')
        greeting = self.selenium.find_element_by_class_name('status-list-header')
        location = greeting.location
        coor_X = location['x']
        coor_Y = location['y']
        self.assertEqual(coor_X, 86)
        self.assertEqual(coor_Y, 326)
