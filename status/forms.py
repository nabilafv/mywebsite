from django import forms

class Status_Form(forms.Form):

    attrs = {
        'class': 'form-control',
        'placeholder': 'Express your thoughts here <3',
    }

    status = forms.CharField(label='', required=True, max_length=300, widget=forms.Textarea(attrs=attrs))