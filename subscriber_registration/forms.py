from django import forms

attrs_fullName = {
    'class': 'form-control',
    'placeholder': 'Full name',
    'pattern': '[A-Za-z ]+',
    'title': 'Only alphabetic characters are allowed.',
}

attrs_email = {
    'class': 'form-control',
    'placeholder': 'E-mail',
}

attrs_password = {
    'class': 'form-control',
    'placeholder': 'Password',
}

class Subscriber_Registration_Form(forms.Form):
    full_name = forms.CharField(label="", required=True, max_length=70, widget=forms.TextInput(attrs=attrs_fullName))
    email = forms.EmailField(label="", required=True, widget=forms.EmailInput(attrs=attrs_email))
    password = forms.CharField(label="", max_length=128, widget=forms.PasswordInput(attrs=attrs_password))
