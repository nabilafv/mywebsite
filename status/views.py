from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status

# Create your views here.

response = {}

def index(request):
    allStatus = Status.objects.all().values().filter().order_by('created_at').reverse()
    if (request.method == 'POST'):
        form = Status_Form(request.POST)
        if (form.is_valid()):
            response['status'] = request.POST['status']
            status = Status(status=response['status'])
            status.save()
            HttpResponseRedirect('/')
    else:
        form = Status_Form()
    response['form'] = form
    response['allStatus'] = allStatus
    return render(request, 'status.html', response)