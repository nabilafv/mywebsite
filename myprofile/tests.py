from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time
import unittest

# Create your tests here.
class Status_Test(TestCase):
    def test_status_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_status_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
