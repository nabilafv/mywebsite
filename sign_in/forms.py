from django import forms

attrs_email = {
    'class': 'form-control',
    'placeholder': 'E-mail',
    'type': 'text',
}

attrs_password = {
    'class': 'form-control',
    'placeholder': 'Password',
    'type': 'password',
}

class Sign_In_Form(forms.Form):
    email = forms.EmailField(label="", required=True, widget=forms.EmailInput(attrs=attrs_email))
    password = forms.CharField(label="", max_length=128, widget=forms.PasswordInput(attrs=attrs_password))
