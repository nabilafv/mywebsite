from django.db import models

# Create your models here.
class Subscriber(models.Model):
    full_name = models.CharField(max_length=70)
    email = models.EmailField()
    password = models.CharField(max_length=128)