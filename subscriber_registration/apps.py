from django.apps import AppConfig


class SubscriberRegistrationConfig(AppConfig):
    name = 'subscriber_registration'
