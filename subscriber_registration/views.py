from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .forms import Subscriber_Registration_Form
from .models import Subscriber
import json

# Create your views here.
@csrf_exempt
def index(request):
    subscribers = Subscriber.objects.all().values()
    if (request.method == 'POST'):
        form = Subscriber_Registration_Form(request.POST)
        if (form.is_valid()):
            full_name = request.POST['full_name']
            email = request.POST['email']
            password = request.POST['password']
            subscriber = Subscriber(full_name=full_name, email=email, password=password)
            subscriber.save()
    else:
        form = Subscriber_Registration_Form()
    form = form
    return render(request, 'subscriber_registration.html', {'form': form, 'subscribers': subscribers})

@csrf_exempt
def validate(request):
    if request.method == "POST":
        email = request.POST['email']
        if Subscriber.objects.filter(email=email).exists():
            error_message = '{} has already been registered. Please try another e-mail address.'.format(email)
            return HttpResponse(json.dumps({'message': error_message}), content_type="application/json")
        return HttpResponse(json.dumps({'message': 'Email has not been registered yet.'}), content_type="application/json")
    else:
        return HttpResponse(json.dumps({'message': 'Error'}), content_type="application/json")

@csrf_exempt
def unsubscribe(request, password):
    subscriber = Subscriber.objects.filter(password=password)
    if Subscriber.objects.filter(password=password).exists():
        message = 'You have been unsubscribed!'
        subscriber.delete()
        return HttpResponse(json.dumps({'message': message}), content_type="application/json")
    else:
        return HttpResponse(json.dumps({'message': 'Wrong password. Please try again.'}), content_type="application/json")
