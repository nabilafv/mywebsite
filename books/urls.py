from django.conf.urls import url
from django.urls import path
from .views import index, getJSON, defaultJSON, signOut, addCounter, decCounter

urlpatterns = [
    path('', index, name='index'),
    path('getDefaultJSON/', defaultJSON, name='defaultJSON'),
    path('fetchData/<str:title>', getJSON, name='getJSON'),
    path('signOut/', signOut, name='signOut'),
    path('addCounter/', addCounter, name='addCounter'),
    path('decCounter/', decCounter, name='decCounter'),
]