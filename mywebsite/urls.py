"""mywebsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('social_django.urls', namespace='social')),
    path('api-auth/', include(('rest_framework.urls'), namespace='rest_framework')),
    path('', include(('myprofile.urls', 'myprofile'), namespace='myprofile')),
    path('status/', include(('status.urls', 'status'), namespace='status')),
    path('books/', include(('books.urls', 'books'), namespace='books')),
    path('registration/', include(('subscriber_registration.urls', 'subscriber_registration'), namespace='subscriber_registration')),
    path('sign-in/', include(('sign_in.urls', 'sign_in'), namespace='sign_in')),
]