from django.urls import path
from .views import index, validate, unsubscribe

urlpatterns = [
    path('', index, name='index'),
    path('validate/', validate, name='validate'),
    path('unsubscribe/<str:password>', unsubscribe, name='unsubscribe'),
]
