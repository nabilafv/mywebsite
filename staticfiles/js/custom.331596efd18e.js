$(document).ready(
    function () {
        $("#accordion").accordion({
            header: "h3",
            active: false,
            collapsible: true,
            heightStyle: "content",
            navigation: true
        });

        var alreadyClicked = false;
        $("#change-theme").click(function () {
            if (!alreadyClicked) {
                $("#theme").prop("href", "/static/css/dark-style.css");
                $("#accordion-theme").prop("href", "//code.jquery.com/ui/1.12.1/themes/ui-darkness/jquery-ui.css");
                alreadyClicked = true;
            } else {
                $("#theme").prop("href", "/static/css/style.css");
                $("#accordion-theme").prop("href", "//code.jquery.com/ui/1.12.1/themes/hot-sneaks/jquery-ui.css");
                alreadyClicked = false;
            }
        });

        $(window).load(function () {
            // Animate loader off screen
            $(".loading").delay(1200).fadeOut("slow");
        });

        $("input").change(function () {
            var search = $("input").val();
            $("#search-books-title").click(function () {
                $("#counts").html('0');
                countBookmark = 0;
                $.ajax({
                    url: "/books/fetchData/" + search,
                    success: function (data) {
                        var html_to_append = "";
                        var books = data.items;
                        var counter = 1;
                        for (var i = 0; i < books.length; i++) {
                            html_to_append += '<tbody><tr>';
                            html_to_append += '<td>' + counter + '</td>';
                            html_to_append += '<td>' + data.items[i].volumeInfo.title + '</td>';
                            html_to_append += '<td>' + data.items[i].volumeInfo.authors + '</td>';
                            html_to_append += '<td>' + data.items[i].volumeInfo.publisher + '</td>';
                            html_to_append += '<td>' + data.items[i].volumeInfo.publishedDate + '</td>';
                            html_to_append += '<td>' + data.items[i].volumeInfo.categories + '</td>';
                            html_to_append += "<td>" + "<a href='" + data.items[i].volumeInfo.infoLink
                                + "'><img src='" + data.items[i].volumeInfo.imageLinks.thumbnail + "'></a></td>";
                            html_to_append += '<td align="center"><a href="#" class="btn btn-default bookmarkButton"><i class="fas fa-star fa-lg"></i></a></td>';
                            html_to_append += '</tr></tbody>';
                            counter++;
                        }
                        $("#books-table > tbody").empty();
                        $("#books-table").append(html_to_append);
                    },
                })
            });
        });

        $.ajax({
            url: "/books/getDefaultJSON/",
            success: function (data) {
                var html_to_append = "";
                var books = data.items;
                var counter = 1;
                for (var i = 0; i < books.length; i++) {
                    html_to_append += '<tbody><tr>';
                    html_to_append += '<td>' + counter + '</td>';
                    html_to_append += '<td>' + data.items[i].volumeInfo.title + '</td>';
                    html_to_append += '<td>' + data.items[i].volumeInfo.authors + '</td>';
                    html_to_append += '<td>' + data.items[i].volumeInfo.publisher + '</td>';
                    html_to_append += '<td>' + data.items[i].volumeInfo.publishedDate + '</td>';
                    html_to_append += '<td>' + data.items[i].volumeInfo.categories + '</td>';
                    html_to_append += "<td>" + "<a href='" + data.items[i].volumeInfo.infoLink
                        + "'><img src='" + data.items[i].volumeInfo.imageLinks.thumbnail + "'></a></td>";
                    html_to_append += '<td align="center"><a href="#" class="btn btn-default bookmarkButton"><i class="fas fa-star fa-lg"></i></a></td>';
                    html_to_append += '</tr></tbody>';
                    counter++;
                }
                $("#books-table").append(html_to_append);
            },
        });

        var timer;
        $("#id_email").keyup(function (e) {
            clearTimeout(timer);
            timer = setTimeout(function () {
                validateEmail();
            }, 2000);
        });

        function validateEmail() {
            $.ajax({
                url: "/registration/validate/",
                type: "POST",
                data: {
                    email: $('#id_email').val(),
                    full_name: $('#id_full_name').val(),
                    password: $('#id_password').val()
                },
                success: function (response) {
                    if (response.message == "Email has not been registered yet.") {
                        $('#sign-up-btn').prop('disabled', false);
                        alert(response.message);
                    } else {
                        $('#sign-up-btn').prop('disabled', true);
                        alert(response.message);
                    }
                },
                error: function (errmsg) {
                    console.log(errmsg);
                }
            });
        }

        $.ajax({
            url: "/registration/",
            type: "POST",
            data: {
                email: $('#id_email').val(),
                full_name: $('#id_full_name').val(),
                password: $('#id_password').val()
            },

            success: function (json) {
                console.log(json);
                $('#subscriber-form').val('');
            },
        });

        $('#subscriber-form').submit(function () {
            alert("Hello, my new subscriber!\nYour data has been successfully saved :3");
        });

        $("td #unsubscribeButton").click(function () {
            confirmDelete();
        });

        function confirmDelete() {
            if (confirm("Are you sure you want to unsubscribe?")) {
                confirmPassword();
            }
        }

        function confirmPassword() {
            var password = prompt("Please enter your password:", "");
            if (password != null || password != "") {
                $.ajax({
                    url: "/registration/unsubscribe/" + password,
                    success: function (response) {
                        alert(response.message);
                    },
                });
                location.reload();
            }
        }
});

var countBookmark = 0;
$(document).on('click','.bookmarkButton',function(){
    if ($(this).hasClass("bookmarkButtonClicked"))
        countBookmark--;
    else
        countBookmark++;
    $(this).toggleClass("bookmarkButtonClicked");
    $("#counts").html(countBookmark);
});

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  $("#pic").attr('src', profile.getImageUrl());
  $("#email").text(profile.getEmail());
  $("#name").html('Hello, ' + profile.getName() + '!');
  $("#name2").html(profile.getName() + ', please select your favorite books.');
}

 function onLoad() {
      gapi.load('auth2,signin2', function() {
        var auth2 = gapi.auth2.init();
        auth2.then(function() {
          // Current values
          var isSignedIn = auth2.isSignedIn.get();
          var currentUser = auth2.currentUser.get();

          if (!isSignedIn) {
            // Rendering g-signin2 button.
            gapi.signin2.render('google-signin-button', {
              'onsuccess': 'onSignIn'
            });
          }
        });
      });
    }

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function() {
        alert("You have been successfully signed out!");
        console.log('User signed out.');
    });
}

var auth2;

/**
 * Initializes the Sign-In client.
 */
var initClient = function() {
    gapi.load('auth2', function(){
        /**
         * Retrieve the singleton for the GoogleAuth library and set up the
         * client.
         */
        auth2 = gapi.auth2.init({
            client_id: '602226791509-abivehbi7a4qjejha826d61h07v3udkj.apps.googleusercontent.com'
        });

        // Attach the click handler to the sign-in button
        auth2.attachClickHandler('signin-button', {}, onSuccess, onFailure);
    });
};