from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
import requests
from django.contrib.auth import logout

# Create your views here.
from django.views.decorators.csrf import csrf_exempt

response = {}
response['counter'] = 0

def getJSON(request, title):
    data = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + title)
    return HttpResponse(data, content_type='application/json')

def defaultJSON(request):
    data = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
    return HttpResponse(data, content_type='application/json')

def index(request):
    if request.user.is_authenticated:
        if 'counter' not in request.session:
            request.session['counter'] = 0
        response['counter'] = request.session['counter']
    else:
        request.session['counter'] = 0
    return render(request, "books.html", response)

@csrf_exempt
def addCounter(request):
    if request.user.is_authenticated:
        request.session['counter'] = request.session['counter'] + 1
    return HttpResponse(request.session['counter'], content_type='application/json')

@csrf_exempt
def decCounter(request):
    if request.user.is_authenticated:
        request.session['counter'] = request.session['counter'] - 1
    return HttpResponse(request.session['counter'], content_type='application/json')

def signOut(request):
    request.session.flush()
    logout(request)
    return HttpResponseRedirect("/")