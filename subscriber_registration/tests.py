from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import Subscriber
from .forms import Subscriber_Registration_Form

# Create your tests here.
class Subscriber_Test(TestCase):
    def test_registraion_url_is_exist(self):
        response = Client().get('/registration/')
        self.assertEqual(response.status_code, 200)

    def test_registration_using_index_func(self):
        found = resolve('/registration/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_subscriber(self):
        # Creating a new user
        new_subscriber = Subscriber.objects.create(full_name='Nabila Febri Viola', email='nabila.febri@ui.ac.id', password='uwaw33')

        # Retrieving all available user
        counting_all_available_subscriber = Subscriber.objects.all().count()
        self.assertEqual(counting_all_available_subscriber, 1)

    def test_form_validation_for_blank_items(self):
        form = Subscriber_Registration_Form(data={'full_name': '', 'email':'', 'password': ''})
        self.assertFalse(form.is_valid())

    def test_form(self):
        form = Subscriber_Registration_Form(data={'full_name': 'Nabila Febri Viola', 'email':'nabila.febri@ui.ac.id', 'password': 'uwaw33'})
        self.assertTrue(form.is_valid())

    def test_post(self):
        response = self.client.post('/registration/', {'full_name': 'Nabila Febri Viola', 'email':'nabila.febri@ui.ac.id', 'password': 'uwaw33'})
        self.assertEqual(response.status_code, 200)
