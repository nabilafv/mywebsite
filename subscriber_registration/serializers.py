from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers
from .models import Subscriber


class SubscriberSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = Subscriber
        fields = ('full_name', 'email', 'password')

    def validate_email(self, email):
        try:
            Subscriber.objects.get(email=email)
        except ObjectDoesNotExist:
            return email
        raise serializers.ValidationError('Email is already taken.')